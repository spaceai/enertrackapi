const jwt = require('jsonwebtoken')

const options = { expiresIn: '1d', issuer: process.env.JWT_ISSUER || 'http://localhost' }

module.exports = {
    sha1: (str) => {
        const buffer = new TextEncoder('utf-8').encode(str)
        const digest = require('crypto').createHash('sha1').digest(buffer)
        // Convert digest to hex string
        return Array.from(new Uint8Array(digest)).map( x => x.toString(16).padStart(2, '0')).join('')
    },
    signToken: (payload) => {
        return jwt.sign(payload, process.env.JWT_SECRET, options)
    },
    validateToken: (req, res, next) => {
console.log('Headers:', req.headers)
        if (! req.headers.authentication) 
        return res.status(401).send({ error: `Authentication error: Token required.` })

        const[ type, token ] = req.headers.authentication.split(' ')

        if (type!='Bearer') 
        return res.status(401).send({ error: `Authentication error: Wrong type.` })

        try {
            // verify makes sure that the token hasn't expired and has been issued by us
            payload = jwt.verify(token, process.env.JWT_SECRET, options);

            // Let's pass back the decoded token to the request object
            req.payload = payload;
            // We call next to pass execution to the subsequent middleware
            next();
            return
        } 
        catch (err) {
            // Throw an error just in case anything goes wrong with verification
            // throw new Error(err);
            res.status(401).send({ error: err })
        }
    }
}
