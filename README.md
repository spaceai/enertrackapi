     "178",        | '        A.Ms.Amp' | '0.098' (A)
     "180",        | '        A.Ms.Vol' | '238.620' (V)
     "182",        | '       A.Ms.Watt' | '23.000' (W)
     "221",        | '         E-Total' | '1631.815' (kWh)
     "224",        | '       Mt.TotTmh' | '349.537' (h)
     "225",        | '     Mt.TotOpTmh' | '329.431' (h)

6aad102b24649c6e3bec693eac7692ba8ab574cf ??

# server
## data model
locations: { id, name, lat, long }
devices: { id, name, locationId, rockblock: serial }
logs: { id, ts, deviceId, values... }

## API
POST /auth { user, pass } _validates user + SHA1(pass) in table *auth* and creates a JWT token_

# client
## routes
/auth _form to send { user, pass } and receive a token (to *sessionStorage*)_

/locations _show map of allowed locations (by user) and the graph could show average V, A and sum of W_

/locations/:L _or_
/locations/:L/devices _show list of devices in location and average / sum_

/locations/:L/devices:D _or_
/devices/:D _show current data from chosen device (like now)_

# RockBLOCK
## sample
178=0.082&180=235.380&182=19.000&221=1631.815&224=349.401&225=329.431&ts=20181026111600

(120 bytes tops)

_To store Latitude and Longitude, you can use DECIMAL datatype in MYSQL._
_To be specific, use DECIMAL(10, 8) for Latitudes as it range from -90 to +90 (degree), and for longitude you can use DECIMAL(11, 8) as its range is from -180 to +180 (degree)._
