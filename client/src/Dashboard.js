import React, { Fragment } from 'react'
import { Link } from '@reach/router'

import { ContextConsumer } from './Context'

import Gauge from './Gauge'


export default ({ id }) => (
    <ContextConsumer>
        {({ devices, getDevice, measure, getMeasure, setMeasure }) => {
            const device = devices.find( f => f.id===parseInt(id, 10))

            if (! device.daily) {
                console.log('Fetching Device', id)
                getDevice(id)
                return <div className="alert alert-info"><i class="fas fa-bowling-ball fa-spin"></i> Cargando...</div>
            }
            const selected = getMeasure()

            const Button = ({ tag }) => (
                <input type="button" className={"btn btn-secondary"+(measure===tag ? "active" : "")}
                    value={ getMeasure(tag).btn }
                    onClick={() => setMeasure(tag)} 
                />
            )
        
            return (
                <Fragment>
                    <h4 className="row alert alert-primary transparent-alert text-center">
                        <Link className="float-left" to={`/locations/${device.location_id}`}>
                            <i className="fas fa-angle-up"></i>
                        </Link>
                        <div className="col">{ device.name } &nbsp; {device.daily.day }</div>
                    </h4>
        
                    <div className="row boxShadow boxBackground">
                        <div id="gaugeContainer" className="col-lg-5 col-md-12 px-2">
                            <h4>{ selected.label } del día</h4>
        
                            <Gauge value={ device.daily[selected.key] } 
                                minValue={ selected.minValue }
                                maxValue={ selected.maxValue }
                            />
                        </div>
        
                        <div className="col-lg-7 col-md-12">
                            <div id="buttonGroup" className="btn-group mb-4" role="group" aria-label="Selector">
                                <Button tag="KW" />
                                <Button tag="KV" />
                                <Button tag="KA" />
                            </div>

                            <p>{ selected.label } última semana: { device.weekly[selected.key] }</p>
                            <p>{ selected.label } último mes: { device.monthly[selected.key] }</p>
                            <p>&nbsp;</p>
                            <p>Total histórico: xxxxx</p>
                        </div>
                    </div>
                </Fragment>
            )
        }}
    </ContextConsumer>    
)