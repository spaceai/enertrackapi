import React, { Fragment } from 'react'
import { Router, Link } from '@reach/router'

import { ContextConsumer } from './Context'

import Dashboard from './Dashboard'


const Devices = ({ devices }) => {
    return (
        <Fragment>
            <h4 className="row alert alert-primary transparent-alert">
                <Link className="float-left" to="/locations">
                    <i className="fas fa-angle-left"></i>
                </Link>
                <div className="col">Inversores</div>
            </h4>

            <div className="row">
            { devices.map( m => 
                <Link key={m.id} className="btn btn-block btn-secondary" to={`devices/${m.id}`}>{ m.name }</Link>
            )}
            </div>
        </Fragment>
    )
}
const NotFound = props => <div className="alert alert-warning">Route not found - { JSON.stringify(props) }</div>

const Layout = ({ devices }) => {
    return (
        <Router primary={ false }>
            <Devices path="/" devices={ devices } />
            <Dashboard path="devices/*" />
            <NotFound default />
        </Router>
    )
}


export default ({ id }) => (
    <ContextConsumer>
        {({ locations, getLocations }) => {
            id = parseInt(id, 10)

            // if (! locations.find( f => f.id===id )) {
            //     console.log('Fetching Location', id)    // if not exists? infinite loop? TODO: test
            //     getLocations(id)
            //     return null
            // }
            if (! locations.find( f => f.id===id ).devices) {
                console.log('Fetching Location', id)    // if not exists? infinite loop? TODO: test
                getLocations(id)
                return null
            }
            const devices = locations.find( f => f.id===id ).devices

            console.log('Rendering Devices', devices)
            
            return <Layout devices={ devices } />
        }}
    </ContextConsumer>
)
