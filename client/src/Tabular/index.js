import React from 'react'

import { ContextConsumer } from '../Context'

export default ({ match }) => (
    <ContextConsumer>
        {(props) => {
            if (props.device) {
                return <Tabular {...match } {...props } />
            } else {
                props.getDevice()
                return null
            }
        }}
    </ContextConsumer>
)

function Tabular({ device:{ log }}) {
    if (! log)
        return null;
    
    return (
        <div className="row">
            <div id="tableTitle" className="boxLightBackground boxShadow"><div>Inversor 1</div></div>
            <table className="table text-right boxBackground boxShadow tableStyle">
                <thead>
                    <tr><th>Dia</th>
                        <th>Inverter</th>
                        <th>A.Ms.Amp</th>
                        <th>A.Ms.Vol</th>
                        <th>A.Ms.Watt</th>
                        <th>Mt.TotTmh</th>
                    </tr>
                </thead>

                <tbody>
                { log.map( m => (
                        <tr key={ m.day }>
                            <td className="text-center">{ m.day }</td>
                            <td className="text-center">{ m.device }</td>
                            <td>{ m['A.Ms.Amp'] }</td>
                            <td>{ m['A.Ms.Vol'] }</td>
                            <td>{ m['A.Ms.Watt'] }</td>
                            <td>{ m['Mt.TotTmh'] }</td>
                        </tr>
                ))}
                </tbody>
            </table>
        </div>
    )
}
