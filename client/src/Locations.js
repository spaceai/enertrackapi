import React, { Fragment } from 'react'
import { Router, Link } from '@reach/router'

import { ContextConsumer } from './Context'

import Map from './Map'
import Devices from './Devices'
import Chart from './Chart'


const Locations = ({ locations }) => (
    <div className="mx-2 my-2">
        { locations.map( m => 
            <Link key={m.id} className="btn btn-block btn-secondary" to={`/locations/${m.id}`}>{ m.name }</Link>
        )}
    </div>
)
const ChartWrapper = ({ id }) => (
    <ContextConsumer>
        {({ devices, getDevice, getMeasure }) => {
            const device = devices.find( f => f.id===parseInt(id, 10))

            if (! device || ! device.daily) {
                console.log('Fetching Device', id)
                getDevice(id)
                return <div className="alert alert-info"><i class="fas fa-bowling-ball fa-spin"></i> Cargando...</div>
            }
            const selected = getMeasure()

            return <Chart device={ device } selected={ selected } />
        }}
    </ContextConsumer>
    
)
const Layout = ({ locations }) => (
    <Fragment>
        <div className="row text-center">
            <div className="col-lg-4 col-md-6 col-sm-12" id="mapContainer">
                <div className="card h-100 boxShadow" style={{ backgroundColor: "rgba(255,255,255,0.1)" }}>
                    <Router primary={ false }>
                        <Locations path="/" locations={ locations } />

                        <Map path=":id/*" locations={ locations } />
                    </Router>
                </div>
            </div>

            <div className="col-lg-8 col-md-6 col-sm-12">
                <Router primary={ false }>
                    <Devices path=":id/*" />
                </Router>
            </div>
        </div>

        <div className="row">
            <Router primary={ false } className="col">
                <ChartWrapper path=":stub/devices/:id" />
            </Router>
        </div>
    </Fragment>
)


export default () => (
    <ContextConsumer>
        {({ locations, getLocations }) => {
            if (! locations.length) {
                console.log('Fetching Locations on F5', locations)
                getLocations()
                return null
            }
            console.log('Rendering Locations', locations)
            
            return <Layout locations={ locations } />
        }}
    </ContextConsumer>
)
