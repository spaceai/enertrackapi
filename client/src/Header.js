import React, { PureComponent } from 'react'
import { Link } from '@reach/router'

export default class Header extends PureComponent {
    state = {
        isCollapsed: true
    }
    toggleCollapse = () => this.setState( state => ({ isCollapsed: ! state.isCollapsed }))

    handleSubmit = (event) => {
        event.preventDefault()

        return this.props.forget()
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark text-light mb-2 boxShadow">
                <span className="navbar-brand mb-0 h1">
                    <img className="logo" src={ process.env.PUBLIC_URL +"/logo_200.png"} alt="SpaceAI logo" />
                </span>

                <button className="navbar-toggler" type="button" onClick={ this.toggleCollapse }>
                    <span className=""><i className="fas fa-bars fa-1x"></i></span>
                </button>

                <div className={"collapse navbar-collapse "+(this.state.isCollapsed ? "" : "show")}>
                    <div className="lead title">EnerTrack</div>

                    <div className="navbar-nav mr-auto">
                        <Link to="locations" className="nav-item nav-link">
                            <i className="fas fa-tachometer-alt"></i> &nbsp; Tablero
                        </Link>
                        <Link to="tabular" className="nav-item nav-link">
                            <i className="fas fa-list"></i> &nbsp; Registros
                        </Link>
                    </div>

                    <form className="form-inline my-2 my-lg-0" onSubmit={ this.handleSubmit }>
                        <button className="btn logout my-2 my-sm-0" type="submit">
                            { this.props.user } &nbsp; <i className="fas fa-sign-out-alt"></i>
                        </button>
                    </form>
                </div>
            </nav>
        )
    }
}
