import React, { PureComponent } from 'react'
import axios from 'axios'

axios.defaults.baseURL = process.env.BASE_URL || '/'

const Context = React.createContext()


export const ContextConsumer = Context.Consumer
export class ContextProvider extends PureComponent {
    state = {
        user:  sessionStorage.user  || '',
        token: sessionStorage.token || '',

        locations: [],
        devices: [],

        currentLocation: { id: 0 },
        currentDevice: { id: 0 },
        
        measure: 'KV',
    }

    persist = () => ['user', 'token', 'locations'].reduce((r, c) => { r[c] = this.state[c]; return r }, {})

    handles = {
        //  Auth
        connect: async (user, pass) => {
            const { data } = await axios
            // .get(`/auth?user=${user}&pass=${pass}`)
            .post(`/auth`, { user, pass })
            .catch( err => console.log(err))    // TODO: catch 401
    
            if (! data) return false
            
            this.setState({ user, ...data })
            return true
        },        
        forget: () => {
            sessionStorage.clear()
            this.setState({ user: '', token: '', locations: [], devices: [] })
            return false
        },
        //  Locations
        getLocations: (id) => {
            if (! id) {
                axios.get('/locations')
                .then( done => done.data )
                .then( locations => this.setState({ locations }))
                .catch( err => console.log(err))
            }
            else {
                axios.get('/locations/'+ id)
                .then( done => done.data )
                .then( location => {
                    const locations = this.state.locations
                    .filter( f => f.id!==parseInt(id, 10) )
                    .concat(location)

                    const devices = this.state.devices
                    .filter( f => f.location_id!==parseInt(id, 10))
                    .concat(location.devices)

                    this.setState({ locations, devices })
                })
                .catch( err => console.log(err))
            }
        },
        //  Devices
        getDevice: (id) => {
            axios.get(`/devices/${id}`)
            .then( done => done.data )
            .then( device => {
                const devices = this.state.devices
                .filter( f => f.id!==parseInt(id, 10))
                .concat(device)

                this.setState({ devices })
            })
            .catch( err => console.log(err))
        },
        setMeasure: (measure) => this.setState({ measure }),
        getMeasure: (measure) => {
            const measures = { 
                KA:{ btn: 'Amp', label: 'Amps' , key: 'A.Ms.Amp' , minValue: .01, maxValue: .11 }, 
                KV:{ btn: 'Vol', label: 'Volts', key: 'A.Ms.Vol' , minValue: 100, maxValue: 300 }, 
                KW:{ btn: 'Wtt', label: 'Watts', key: 'A.Ms.Watt', minValue:  10, maxValue:  40 }
            }
            return measures[measure || this.state.measure]
        }
    }
    render() {
        // TODO: review
        console.log('Auth token and render', this.state.token)
        axios.defaults.headers.common = { Authentication: 'Bearer '+ this.state.token }
		return (
			<Context.Provider value={{ ...this.state, ...this.handles }}>
				{ this.props.children || null }
			</Context.Provider>
		)
    }
	componentDidCatch(error, info) {
		// Display fallback UI
		this.setState({ hasError: true })
		// You can also log the error to an error reporting service
		console.log(error, info)
	}
	componentDidMount() {
        const hydrated = ['user', 'token']
		.filter( f => sessionStorage[f] )
        .map( m => ({ [m]: sessionStorage[m] }))
        // const hydrated = JSON.parse(sessionStorage.persist)

        this.setState(hydrated)

        // axios.interceptors.response.use( 
        //     response => response, 
        //     error => {
        //         if (error.response.status===401) {
        //             error.config.__retry = false
        //             this.handles.forget()
        //         }
        //         return error    // Promise.reject(error)
        //     }
        // )

        // if (this.state.token) {
        //     this.handles.getLocations()
        //     this.handles.getDevice()
        // }
	}
	componentDidUpdate(prevProps, prevState) {
        // persist these states
        ['user', 'token']
		.filter( f => prevState[f]!==this.state[f])
        // .map( m => (sessionStorage[m] = JSON.stringify(this.state[m])))
        .map( m => (sessionStorage[m] = this.state[m]))

        if (prevState.token!==this.state.token) {
            if (this.state.token) {
                axios.defaults.headers.common = { Authentication: 'Bearer '+ this.state.token }
            }
            else {
                delete axios.defaults.headers.common.Authentication
            }
        }
        // console.log('persist', this.persist())
	}
}
