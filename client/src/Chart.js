import React from 'react'
import { VictoryChart, VictoryAxis, VictoryLine } from 'victory'

const axisStyles = { 
    tickLabels:{ fontSize: 6 }, 
    ticks:{ stroke: 'gray', size: 1 },
    grid:{ stroke: 'gray' }
}
const lineStyles = { data:{ stroke: '#004085', strokeWidth: 1 }}

export default function Chart({ device, selected }) {
    const data = device.log.map( m => ({ day: m.day, val: m[selected.key] }))
    const labelY = selected.label
    console.log('Chart', data, labelY)
    return (
        <div className="row">
            <div id="historicalChart" className="col-12 boxShadow boxBackground">
                <VictoryChart animate={{ duration: 500, onLoad:{ duration: 500 }}} domainPadding={ 20 } height={ 200 }>
                    <VictoryAxis tickFormat={(ymd) => (ymd.slice(8, 10))}  style={ axisStyles } 
                        label="Día" scale={{ x: 'time' }}
                    />
                    <VictoryAxis dependentAxis tickFormat={(val) => (val)} style={ axisStyles } 
                        label={ labelY }
                    />

                    <VictoryLine data={ data } x="day" y="val" style={ lineStyles } />
                </VictoryChart>
            </div>
        </div>
    )
}
