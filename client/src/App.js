import React, { Fragment } from 'react'
import { Router } from '@reach/router'

import { ContextConsumer } from './Context'

import Auth from './Auth'
import Header from './Header'
import Locations from './Locations'

import Tabular from './Tabular/'

const NotFound = props => <div className="alert alert-warning">Route not found - { JSON.stringify(props) }</div>


export default () => (
	<ContextConsumer>
		{({ user, connect, forget }) => (
			<Fragment>
				<div id="back"></div>
				
				<div>
				{	user==='' ?
					<Auth connect={ connect } />
				:
					<Fragment>
						<Header user={ user } forget={ forget } />

						<div className="container">
							<Router>
								<Locations path="/" />
								<Locations path="locations/*" />
								{/* <Devices path="locations/:id" /> */}
								{/* <Graphic path="devices/:id" /> */}
								<Tabular path="tabular" />
								<Auth path="login" />
								<NotFound default />
							</Router>
						</div>
					</Fragment>
				}
				</div>
			</Fragment>
		)}
	</ContextConsumer>
)
