import React from 'react'
import ReactSpeedometer from "react-d3-speedometer"

export default function Gauge({ value, minValue, maxValue }) {
    return (
        <ReactSpeedometer
            value={ value }
            minValue={ minValue } maxValue={ maxValue } forceRender={ true }
            currentValueText={"Current Value: "+ value }
            fluidWidth={ true }
            needleTransitionDuration={ 4000 }
            needleTransition="easeElastic"
        />
    )
}
// https://github.com/palerdot/react-d3-speedometer