import React from 'react'

export default function Auth({ connect }) {
    const userInput = React.createRef()
    const passInput = React.createRef()
    
    const handleSubmit = (event) => {
        event.preventDefault()

        return connect(userInput.current.value, passInput.current.value)
    }

    return (
        <div className="row justify-content-center">
            <div className="card card-login col-sm-6 text-center text-white bg-dark my-3">
                <div className="card-header">
                    <h3 className="card-title display-4">EnerTrack</h3>
                </div>
                <img className="card-img-bottom user-icon" src={ process.env.PUBLIC_URL +'/user.svg' } alt="User" />
                <div className="card-body">
                    <form id="login-form" method="post" onSubmit={ handleSubmit }>
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label h3">User:</label>
                            <div className="col-sm-9">
                                <input className="form-control" ref={ userInput } />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label h3">Pass:</label>
                            <div className="col-sm-9">
                                <input className="form-control" type="password" ref={ passInput } />
                            </div>
                        </div>
                        
                        <div className="form-group row text-center">
                            <div className="offset-sm-3 col-sm-6">
                                <button className="btn btn-block btn-lg btn-dark" type="submit">
                                    Sign In
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
