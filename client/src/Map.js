import React, { PureComponent } from 'react'

// https://itnext.io/google-maps-react-makes-adding-google-maps-api-to-a-react-app-a-breeze-effb7b89e54
import { GoogleApiWrapper, InfoWindow, Map, Marker } from 'google-maps-react'


const MapRenderer = ({ 
    google, selectedPlace, activeMarker, 
    showingInfoWindow, onMapClick, onMarkerClick 
}) => {
    const style = {
        width: '100%', height: '100%',
        marginLeft: 'auto', marginRight: 'auto'
    }
    return (
        <Map item xs={ 12 } style={ style } zoom={ 14 } google={ google }
                initialCenter={ selectedPlace }
                center={ selectedPlace }
                onClick={ onMapClick }>
            <Marker
                name={ 'El Inverter!' }
                position={ selectedPlace }
                title={ 'Inverter' }
                onClick={ onMarkerClick }
            />
            <InfoWindow marker={ activeMarker } visible={ showingInfoWindow }>
                <div>
                    El Inverter! <br />
                    Aquí estamos... <br /> 
                    puedo elegir 1 de los 4 aquí?
                </div>
            </InfoWindow>
        </Map>
    )
}

class MapWrapper extends PureComponent {
    state = {
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: { lat: 19.40886, lng: -99.1787655 }
    }
    handles = {
        //  Map
        onMarkerClick: (props, marker, e) => {
            this.setState({
                selectedPlace: props,
                activeMarker: marker,
                showingInfoWindow: true
            });
        },
        onMapClick: (props) => {
            console.log('Map click', props)
            
            if (this.state.showingInfoWindow) {
                this.setState({
                    showingInfoWindow: false,
                    activeMarker: null
                });
            }
        },
    }
    render() {
        console.log('selectedPlace:', this.state.selectedPlace)
		return (
            <MapRenderer {...this.props } {...this.state } {...this.handles } />
		)
    }
    componentDidMount() {
        const location = this.props.locations.find( f => f.id===parseInt(this.props.id, 10))
        const { lat, lng } = location

        this.setState({ selectedPlace: { lat, lng }})
    }
}
const Loading = () => <div>Cargando...</div>

export default GoogleApiWrapper({
    apiKey: ('AIzaSyBQrKBmS1s8bkEwsTzLglKVwfOlwTAzUTo'),
    language: 'es',
    LoadingContainer: Loading
})(MapWrapper)
