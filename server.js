#!/usr/bin/env nodejs
require('dotenv').config(); // Sets up dotenv as soon as our application starts

const express = require("express");
const app = express();
// const app = require("express")();
const bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

const cors = require('cors')
app.use(cors())

app.use('/auth',      require('./api/auth'))
app.use('/channels',  require('./api/channels'))
app.use('/locations',	require('./api/locations'))
app.use('/devices', 	require('./api/devices'))
app.use('/logs',	    require('./api/logs'))
app.use('/rock7',	    require('./api/rock7'))
//
//  client
//
const path = require('path')

if (true || process.env.NODE_ENV==='production') {
  app.use(express.static(path.join(__dirname, 'client/build')));
  // Handle React routing, return all requests to React app
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}
//
//  listen to all
//
const port = process.env.NODE_ENV=='development' ? 5001 : (process.env.PORT || 5000)

app.listen(port, () => {
  console.log(`Server running on port ${port}`, process.env.NODE_ENV);
});
