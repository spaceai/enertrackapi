const router = require('express').Router()
const fs = require('fs')

const pool = require('../database')

router.get('/', (req, res) => {
	try {
		const channels = JSON.parse(fs.readFileSync('channels.json'))

		channels
			.map( chn => ({ id: chn.channel, label: chn.label }))
			.map( chn => pool
				.query(`INSERT INTO channels SET ?`, chn)
				.catch( error => { throw error })
			)

		res.json(channels);
	} 
	catch (err) {
		console.log('api/channels error', err)
	}
});
  
module.exports = router
