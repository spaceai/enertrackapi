const router = require('express').Router()

// OMIT router.use(require('../auth').validateToken)

const db = require('../db')

// https://stackoverflow.com/questions/52261494/hex-to-string-string-to-hex-conversion-in-nodejs
const convert = (from, to) => str => Buffer.from(str, from).toString(to)
const utf8ToHex = convert('utf8', 'hex')
const hexToUtf8 = convert('hex', 'utf8')
// hexToUtf8(utf8ToHex('dailyfile.host')) === 'dailyfile.host'

router.post('/', async (req, res) => {
	console.log('From Rock7 came', req.body)
	
	const data = hexToUtf8(req.body.data)
	
	const { insertId } = await db
	.query(`INSERT INTO logs SET ?`, data)
	.catch( error => { throw error })

    res.send(`OK: ${insertId}`)	// rock7 expects status 200 from POST
})

module.exports = router
