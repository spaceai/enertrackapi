CREATE OR REPLACE VIEW monthly
AS
SELECT W.thruDay AS day, D.device, D.device_id, D.location_id
, SUM(`A.Ms.Amp`)    AS `A.Ms.Amp`
, SUM(`A.Ms.Vol`)    AS `A.Ms.Vol`
, SUM(`A.Ms.Watt`)   AS `A.Ms.Watt`
, AVG(`E-Total`)     AS `E-Total`
, AVG(`Mt.TotTmh`)   AS `Mt.TotTmh`
, AVG(`Mt.TotOpTmh`) AS `Mt.TotOpTmh`
FROM daily D 
JOIN (
    SELECT device
    , DATE_ADD(day, INTERVAL -30 DAY) AS fromDay
    , day AS thruDay
    FROM daily
) W ON D.device = W.device AND D.day BETWEEN W.fromDay AND W.thruDay
GROUP BY W.thruDay, D.device, D.device_id, D.location_id
;