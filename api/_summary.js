const db = require('../db')

module.exports = {
    daily: async (query) => {
        let where = ''
        if (query) {
            where = 'WHERE '+ Object.keys(query).map( 
                key => key +' = '+ db.escape(query[key])
            ).join(' AND ')
        }
        const summary = await db
        .query(`
            SELECT DATE_FORMAT(ts, '%Y-%m-%d') AS day
            , ROUND(AVG(`178`), 3) AS `A.Ms.Amp`
            , ROUND(AVG(`180`), 3) AS `A.Ms.Vol`
            , ROUND(AVG(`182`), 3) AS `A.Ms.Watt`
            , ROUND(AVG(`221`), 3) AS `E-Total`
            , ROUND(AVG(`224`), 3) AS `Mt.TotTmh`
            , ROUND(AVG(`225`), 3) AS `Mt.TotOpTmh`
            , COUNT(*) AS records
            FROM logs L
            JOIN devices D ON L.device = D.name
            WHERE ${where}
            GROUP BY DATE_FORMAT(ts, '%Y-%m-%d')
        `)
        .catch( error => { throw error })

        return summary
    }
}