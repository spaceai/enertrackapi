CREATE OR REPLACE VIEW daily
AS
SELECT DATE_FORMAT(ts, '%Y-%m-%d') AS day, D.location_id, G.device_id
, MIN(device) AS device
, ROUND(AVG(`178`), 3) AS `A.Ms.Amp`
, ROUND(AVG(`180`), 3) AS `A.Ms.Vol`
, ROUND(AVG(`182`), 3) AS `A.Ms.Watt`
, ROUND(AVG(`221`), 3) AS `E-Total`
, ROUND(AVG(`224`), 3) AS `Mt.TotTmh`
, ROUND(AVG(`225`), 3) AS `Mt.TotOpTmh`
, COUNT(*) AS records
FROM logs G
JOIN devices D ON G.device_id = D.id
JOIN locations L ON D.location_id = L.id
WHERE `182` > 0
GROUP BY DATE_FORMAT(ts, '%Y-%m-%d'), D.location_id, G.device_id
/* WITH ROLLUP */
;