const router = require('express').Router()

const db = require('../db')

const { signToken } = require('../auth')

// router.get('/', async (req, res) => {
	// const { user, pass } = req.query
router.post('/', async (req, res) => {
	const { user, pass } = req.body
	const [ result ] = await db
	.query('SELECT * FROM auth WHERE user = ? AND pass = SHA1(?)', [user, pass])
	.catch( error => { throw error })

	if (! result) return res.status(401).json({ user, pass })

	//	avoid some round trips
	const [{ locIds }] = await db
	.query(`SELECT GROUP_CONCAT(X.location_id SEPARATOR ',') AS locIds 
			FROM auth_locations X 
			WHERE X.auth_id = ${result.id}`)
	.catch( error => { throw error })

	const [{ devIds }] = await db
	.query(`SELECT GROUP_CONCAT( id SEPARATOR ',') AS devIds
			FROM devices WHERE location_id IN (${locIds})
	`)
	.catch( error => { throw error })

	const locations = await db
	.query(`SELECT * FROM locations WHERE id IN (${locIds})`)
	.catch( error => { throw error })

	const payload = { user, id: result.id, userId: result.id, locIds, devIds, locations }

	res.status(201).json({...payload, token: signToken(payload) })
})

module.exports = router
