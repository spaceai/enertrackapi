const router = require('express').Router()

router.use(require('../auth').validateToken)

const db = require('../db')

router.get('/', async (req, res) => {
	const locIds = req.payload.locIds
	const locations = await db
	.query(`SELECT * FROM locations WHERE id IN (${locIds})`)
	.catch( error => { throw error })
	
	res.json(locations)
})
router.get('/:id', async (req, res) => {
	const { id } = req.params
	const { locIds, devIds } = req.payload

	const [ location ] = await db
	.query(`SELECT * FROM locations WHERE id IN (${locIds}) AND id = ${id}`)
	.catch( error => { throw error })
	
	if (! location) return res.status(403).json({ error: 'Not allowed or the likes'})

	location.devices = await db
	.query(`SELECT * FROM devices WHERE id IN (${devIds}) AND  location_id = ${id}`)
	.catch( error => { throw error })

	// const deviceNames = location.devices
	// .map( m => `'${m.name}'`)
	// .join(', ')
	// const [log, daily, weekly, monthly] = await Promise.all([
	// 	db.query(`SELECT * FROM daily   WHERE device IN (${deviceNames}) AND \`A.Ms.Watt\` > 0 ORDER BY day ASC`), 
	// 	db.query(`SELECT * FROM daily   WHERE device IN (${deviceNames}) AND \`A.Ms.Watt\` > 0 ORDER BY day DESC LIMIT 1`), 
	// 	db.query(`SELECT * FROM weekly  WHERE device IN (${deviceNames}) AND \`A.Ms.Watt\` > 0 ORDER BY day DESC LIMIT 1`), 
	// 	db.query(`SELECT * FROM monthly WHERE device IN (${deviceNames}) AND \`A.Ms.Watt\` > 0 ORDER BY day DESC LIMIT 1`), 
	// ])
	// location.log     = log
	// location.daily   = daily[0]
	// location.weekly  = weekly[0]
	// location.monthly = monthly[0]

	res.json(location)
})

module.exports = router
