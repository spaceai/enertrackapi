const router = require('express').Router()

router.use(require('../auth').validateToken)

const db = require('../db')

router.get('/', async (req, res) => {
	let where = ''
	if (req.query) {
		where = 'WHERE '+ Object.keys(req.query).map( key => key +' = '+ db.escape(req.query[key])).join(' AND ')
	}
	const logs = await db
	.query(`SELECT * FROM logs ${where} ORDER BY device, ts`)		
	.catch( error => { throw error })
	
	res.json(logs)
})

router.post('/', async function (req, res) {
	//	{ device, ts, channels }
	let set = req.body 
	delete set.qs

	const { insertId } = await db
	.query("INSERT INTO logs SET ?", set)
	.catch( error => { throw error })
	
	const results = await db
	.query("SELECT * FROM logs WHERE id = ?", [(insertId || 0)])
	.catch( error => { throw error })

	res.json(results)
})

module.exports = router
