const router = require('express').Router()

router.use(require('../auth').validateToken)

const db = require('../db')

router.get('/', async (req, res) => {
	const devIds = req.payload.devIds
	const devices = await db
	.query(`SELECT * FROM devices WHERE id IN (${devIds})`)
	.catch( error => { throw error })
	
	res.json(devices)
})
router.get('/:id', async (req, res) => {
	const { id } = req.params
	const { devIds } = req.payload

	const [ device ] = await db
	.query(`SELECT * FROM devices WHERE id IN (${devIds}) AND id = ${id}`)
	.catch( error => { throw error })
	
	if (! device) return res.status(403).json({ error: 'Not allowed or the likes'})

	const [log, daily, weekly, monthly] = await Promise
	.all([
		db.query(`SELECT * FROM daily   WHERE device_id = '${id}' ORDER BY day DESC LIMIT 30`), 
		db.query(`SELECT * FROM daily   WHERE device_id = '${id}' ORDER BY day DESC LIMIT 1`), 
		db.query(`SELECT * FROM weekly  WHERE device_id = '${id}' ORDER BY day DESC LIMIT 1`), 
		db.query(`SELECT * FROM monthly WHERE device_id = '${id}' ORDER BY day DESC LIMIT 1`), 
	])
	device.daily   = daily[0]
	device.weekly  = weekly[0]
	device.monthly = monthly[0]
	device.log     = log

	res.json(device)
})

module.exports = router
