require('dotenv').config();

// https://gist.github.com/matthagemann/30cfee724d047007a031eb12b3a95a23

const util = require('util')
const mysql = require('mysql')

const pool = mysql.createPool({
    connectionLimit: 10,
	host:     'localhost',
	user:     process.env.USERNAME,
	password: process.env.PASSWORD,
	database: process.env.DATABASE
})

// Ping database to check for common exception errors.
pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
    }

    if (connection) connection.release()

    return
})
// Promisify for Node.js async/await.
pool.query = util.promisify(pool.query)

module.exports = 
exports = {
	escape: (what) => pool.escape(what)
	,
	query: async (sql, params = []) => await pool
		.query(sql, params)
		.catch( exports.log )
	,
    getWhere: async (table, where) => {
        const filters = Object.keys(where).map( key => `${key} = ${where[key]}`).join(' AND ')
		const results = await pool
		.query(`SELECT * FROM ${table} WHERE ${filters}`)
		.catch( exports.log )

		return results
    },
	get: async (table, ids = null) => {
		const where = ids===null ? '' : `WHERE id IN (${ids})`
		const results = await pool
		.query(`SELECT * FROM ${table} ${where}`)
		.catch( exports.log )
		
		return results
	},
	getFields: async () => {
		const fields = await pool
		.query(`SHOW FIELDS FROM ${table}`)
		.then( fields => fields.map( field => field.Field ))

		return fields
	},
	post: async (table, body) => {
		if (Object.keys(body).length==0) {
			return { code: -400, message: 'No POST data received' }
		}
		const result = { code: -500, message: 'Inserted id' }

		Object.keys(body).map( k => { if (typeof(body[k])==Object) delete body[k] })

		const { insertId } = await pool
		.query(`INSERT INTO ${table} SET ?`, body)
		.catch( error => console.log(error) || (result.message = error ))
		
		result.code = insertId

		return result
	},
	put: async (table, body) => {
		if (Object.keys(body).length==0) {
			return { code: -400, message: 'No PUT data received' }
		}
		const result = { code: -500, message: 'Changed records' }
		const { changedRows } = await pool
		.query(`UPDATE ${table} SET ?`, body)
		.catch( error => console.log(error) || (result.message = error ))
		
		result.code = changedRows

		return result
	},
	del: async (table, id) => {
		const result = { code: -500, message: 'Deleted records' }
		const { affectedRows } = await pool
		.query(`DELETE FROM ${table} WHERE id = ${id}`)
		.catch( error => console.log(error) || (result.message = error ))
		
		result.code = affectedRows

		return result
    },
    log: (what) => console.log(what)
}
